angular.module('myApp',['appRoutes','mainCtrl','userCtrl','authService','userService','storyCtrl','storyService','reverseDirective'])

.config(function($httpProvider){
	$httpProvider.interceptors.push('AuthInterceptor');

	})